# Setting up Project Locally

## Getting started
### Get the code
* Get the code from gitlab ```git clone git@gitlab.com:bunthondev/pos-system.git```
* Jump to project folder ```cd pos-system```
* Create .env file: ```cp .env.example .env```
* Edit your host: ``` vi /etc/hosts ```
* Add domain to your host: 
``` 127.0.0.1   local.pos-system.com ```

### Docker
* Build docker container ```docker-compose build```
* Start container ```docker-compose up -d```

### Working in docker container
* Login to docker container ```docker-compose exec php /bin/bash```
* Install composer dependency: ```composer install```
* Install frontend package: ```yarn install```
* Migrate database: ```php artisan migrate```
* Seed data for testing: ```php artisan db:seed```
* Open Website in your browser: ```local.pos-system.com```
* Open Backpack admin in your browser: ```local.pos-system.com/admin```
