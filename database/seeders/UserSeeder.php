<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $roles = config('settings.roles');

        foreach ($roles as $key => $role) {
            User::firstOrCreate([
                'name' => $role,
                'email' => $key . '@pos-system.com',
                'email_verified_at' => now(),
                'password' => bcrypt('abc12345')
            ]);
        }
    }
}
