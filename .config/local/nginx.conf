load_module /usr/lib/nginx/modules/ngx_http_perl_module.so;

user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;
env ENV;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;
    perl_set $ENV 'sub { return $ENV{"ENV"}; }';

    # This part is a little tricky. Since we're using a setup where the IP of the phpfpm container(s) can change (due to
    # new deployments or an autoscaling event), we cannot allow nginx to cache the IP from DNS on startup. Instead, we
    # need to use a resolver. However, the default AWS resolver 10.0.0.2 wont work on local with docker-compose, so in
    # addition to this, we also run sed in nginx.dev.Dockerfile to replace this.
    resolver 10.0.0.2 valid=60s;

    # Increases the buffer size to avoid issues with the response from PHP being too big
    fastcgi_buffers  128 4096k;
    fastcgi_buffer_size  4096k;

    proxy_buffering on;
    proxy_buffer_size 128k;
    proxy_buffers 4 256k;
    proxy_busy_buffers_size 256k;

    # ----------------------------------------------------------------------
    # | Compression                                                        |
    # ----------------------------------------------------------------------
    gzip on;
    gzip_comp_level 6;
    gzip_proxied any;
    gzip_vary on;
    gzip_types
      application/atom+xml
      application/geo+json
      application/javascript
      application/x-javascript
      application/json
      application/ld+json
      application/manifest+json
      application/rdf+xml
      application/rss+xml
      application/vnd.ms-fontobject
      application/wasm
      application/x-web-app-manifest+json
      application/xhtml+xml
      application/xml
      font/eot
      font/otf
      font/ttf
      image/bmp
      image/svg+xml
      text/cache-manifest
      text/calendar
      text/css
      text/javascript
      text/markdown
      text/plain
      text/xml
      text/vcard
      text/vnd.rim.location.xloc
      text/vtt
      text/x-component
      text/x-cross-domain-policy;

    # Expires map
    map $sent_http_content_type $expires {
        default                    off;
        text/html                  epoch;
        text/css                   30d;
        application/javascript     30d;
        ~image/                    30d;
    }

    server {
        listen 80;
        expires $expires;
        root /var/www/html/public;

        client_max_body_size 512M;

        index index.php;

        location ~* \.(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|rss|atom|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)$ {
            access_log off;
            log_not_found off;
        }

        location / {
            try_files $uri $uri/ /index.php?$query_string;
        }

        location ~ \.php$ {
          try_files $uri /index.php =404;
          fastcgi_pass fpm:9000;
          fastcgi_split_path_info ^(.+\.php)(/.*)$;
          include fastcgi_params;

          # When you are using symlinks to link the document root to the
          # current version of your application, you should pass the real
          # application path instead of the path to the symlink to PHP
          # FPM.
          # Otherwise, PHP's OPcache may not properly detect changes to
          # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
          # for more information).
          fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
          fastcgi_param DOCUMENT_ROOT $realpath_root;
          # Prevents URIs that include the front controller. This will 404:
          # http://domain.tld/app.php/some-path
          # Remove the internal directive to allow URIs like this
          internal;
        }
    }
}
