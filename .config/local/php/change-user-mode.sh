#!/bin/bash

USER_ID=${LOCAL_USER_ID:-1000}

usermod -u $USER_ID -o www-data
groupmod -g $USER_ID -o www-data

exit 0
