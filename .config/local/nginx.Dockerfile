FROM nginx:perl

RUN apt-get update && apt-get install -y curl wget gnupg
WORKDIR /var/www/html
COPY .config/local/nginx.conf /etc/nginx/nginx.conf

RUN sed -i 's/10.0.0.2/127.0.0.11/g' /etc/nginx/nginx.conf
