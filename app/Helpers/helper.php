<?php
use Illuminate\Support\Facades\Schema;

if (! function_exists('modelHasColumn')) {
    function modelHasColumn($model, $column)
    {
        return Schema::hasColumn($model->getTable(), $column);
    }
}