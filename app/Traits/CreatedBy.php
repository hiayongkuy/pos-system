<?php
namespace App\Traits;

trait CreatedBy
{
    protected static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            if (modelHasColumn($model, 'created_by') && $model->created_by === null) {
                $model->created_by = backpack_user()->id;
            }
        });
    }
}
