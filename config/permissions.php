<?php

$adminRole = 'Administrator';
$accountRole = 'Account';
$stockRole = 'Stock';
$user = 'User';

return [
    'product' => [
        'createProduct' => [
            'name'  => 'Create product',
            'roles' => [
                $adminRole
            ]
        ],
        'listProduct' => [
            'name'  => 'List product',
            'roles' => [
                $adminRole
            ]
        ],
        'deleteProduct' => [
            'name'  => 'Delete product',
            'roles' => [
                $adminRole
            ]
        ],
        'updateProduct' => [
            'name'  => 'Update product',
            'roles' => [
                $adminRole
            ]
        ],
    ],
    'invoice' => [
        'createInvoice' => [
            'name'  => 'Create invoice',
            'roles' => [
                $accountRole
            ]
        ],
        'listInvoice' => [
            'name'  => 'List invoice',
            'roles' => [
                $accountRole
            ]
        ],
        'deleteInvoice' => [
            'name'  => 'Delete invoice',
            'roles' => [
                $accountRole
            ]
        ],
        'updateInvoice' => [
            'name'  => 'Update invoice',
            'roles' => [
                $accountRole
            ]
        ],
    ],
];
