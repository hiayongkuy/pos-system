<?php

return [
    'roles' => [
        'admin' => 'Administrator',
        'account' => 'Account',
        'stock' => 'Stock',
        'user' => 'User'
    ]
];
